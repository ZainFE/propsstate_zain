import logo from './logo.svg';
import './App.css';
// import Card from './components/Card/Card';
// import { useState } from 'react';
import Content from './components/Content/Content';

function App() {
  // const [name, setName] = useState("Zainuddin");
  // const [alamat, setAlamat] = useState("Sumbawa");
  return (
    <div className="App">
     <Content />
    </div>
  );
}

export default App;
