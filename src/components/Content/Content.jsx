import React from "react";
import Card from "../Card/Card";
import {Component} from "react";

class Content extends Component {
    constructor() {
        super()
        this.state = {
            name: "Zain",
            alamat: "Sumbawa",
        };
    }
    render() {
        return (
            <>
                <Card namee={this.state.name} alamat={this.state.alamat} />
                <button onClick={() => this.setState({ name: "Zainuddin"})}>
                ubah nama
                </button>
                <button onClick={() => this.setState({ alamat: "NTB"})}>Ubah Alamat</button>
            </>

        );
    }
}

export default Content;